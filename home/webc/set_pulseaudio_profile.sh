#! /bin/bash

echo "Start set_pulseaudio_profile"  > /home/webc/.set_pulseaudio_profile.log
echo " "  >> /home/webc/.set_pulseaudio_profile.log
pulseaudio --check
if [ $? -ne 0 ]; then
	# we assume pulseaudio has not yet started
	# TODO check for errors and do a kill and then start
	echo "pulseaudio is not running; starting pulseaudio..."  >> /home/webc/.set_pulseaudio_profile.log
	echo " "  >> /home/webc/.set_pulseaudio_profile.log
	pulseaudio --start
	# wait briefly for pulseaudio to start
	sleep 5
fi

echo "================================  'pacmd list-cards' start  =================================" >> /home/webc/.set_pulseaudio_profile.log
echo " "  >> /home/webc/.set_pulseaudio_profile.log
pacmd list-cards  >> /home/webc/.set_pulseaudio_profile.log
echo " "  >> /home/webc/.set_pulseaudio_profile.log
echo "================================  'pacmd list-cards' end    =================================" >> /home/webc/.set_pulseaudio_profile.log
echo " "  >> /home/webc/.set_pulseaudio_profile.log

# first or second line of output reads ">>> x card(s) available." where the ">>> " is not always present
nr_cards=$(pacmd list-cards | awk '/card\(s\) available/{print $1; exit}')
if [ "$nr_cards" == ">>>" ]; then
	nr_cards=$(pacmd list-cards | awk '/card\(s\) available/{print $2; exit}')
fi
echo "nr_cards: $nr_cards" >> /home/webc/.set_pulseaudio_profile.log
if [ "$nr_cards" -gt 0 ]; then
	# since NUCs have only a single audio card as will most of the 'simple' signage computers
	# the index of the first card is taken;it on the first line of output that reads
	# "\tindex: x" (not always the first line of output)
	card_index=$(pacmd list-cards | awk '/index:/ {print $2; exit}')
	echo "card_index: $card_index" >> /home/webc/.set_pulseaudio_profile.log
	active_profile=$(pacmd list-cards | awk 'BEGIN{active_profile = ""} /active profile:/ {$1=$2="";active_profile = $0} END{print active_profile}')
	echo "active_profile: $active_profile" >> /home/webc/.set_pulseaudio_profile.log
	# get list of profiles that use HDMI output and have an availability of not "no"
	profiles=$(pacmd list-cards | awk 'BEGIN{profiles = ""; flag = 0} /profiles:/ {flag = 1} /active profile:/ {flag = 0} /.*:.*\(.*/ && flag == 1 {profiles = profiles "\n" $0} END{print profiles}' | awk 'BEGIN{hdmi_profiles = ""} /output:[Hh][Dd][Mm][Ii][^:]*.*available: [^no]/ {hdmi_profiles = hdmi_profiles "\n" $0} END{print hdmi_profiles}' | sed -e '/^[[:space:]]*$/d')
	# profiles=$(pacmd list-cards | awk 'BEGIN{profiles = ""; flag = 0} /profiles:/ {flag = 1} /active profile:/ {flag = 0} /.*:.*\(.*/ && flag == 1 {profiles = profiles "\n" $0} END{print profiles}' | awk 'BEGIN{hdmi_profiles = ""} /output:.*available: [^no]/ {hdmi_profiles = hdmi_profiles "\n" $0} END{print hdmi_profiles}' | sed -e '/^[[:space:]]*$/d')
	echo "profiles:" >> /home/webc/.set_pulseaudio_profile.log
	echo "$profiles" >> /home/webc/.set_pulseaudio_profile.log
	nr_profiles=$(echo "$profiles" | wc -l)
	echo "nr_profiles: $nr_profiles" >> /home/webc/.set_pulseaudio_profile.log

	if ! [[ $active_profile =~ HDMI|hdmi ]]; then
		# no hdmi output in the active profile; see if we can select one
		if [ $nr_profiles -gt 0 ]; then
			# there is at least one HDMI profile
			if [ $nr_profiles -eq 1 ]; then
				profile="$profiles"
			else # take the profile with highest priority
				max_priority=$(echo "$profiles" | awk 'BEGIN{priority=0} match($0,/priority [0-9]+/) {prio=substr($0,RSTART+9,RLENGTH-9); prio=prio+0; prio > priority ? priority=prio : priority=priority;} END{print priority}')
				echo "max_priority: $max_priority" >> /home/webc/.set_pulseaudio_profile.log
				profile=$(echo "$profiles" | awk '/priority '"$max_priority"'/ {print $0; exit}')
			fi
		fi
		echo "profile: $profile" >> /home/webc/.set_pulseaudio_profile.log
		if [[ $profile =~ \+ ]]; then
			profile=$(echo "$profile" | awk 'BEGIN{FS=":"} {print $1":"$2":"$3}')
		else
			profile=$(echo "$profile" | awk 'BEGIN{FS=":"} {print $1":"$2"}')
		fi
		echo "profile: $profile" >> /home/webc/.set_pulseaudio_profile.log
		if [ -n "$profile" ] && [ -n "$card_index"i ]; then
			echo "executing: pacmd set-card-profile $card_index $profile" >> /home/webc/.set_pulseaudio_profile.log
			$(pacmd set-card-profile "$card_index $profile")
		fi
	fi
fi
