#!/bin/bash

. /etc/webc/functions.sh
. /etc/webc/webc.conf

xorg_conf_file_name=/etc/X11/xorg.conf

echo "xorg.conf file name: $xorg_conf_file_name" > /home/webc/.startx.log
echo "basename readlink: $(basename $(readlink /sys/class/graphics/fb0/device/driver))" >> /home/webc/.startx.log
echo "cat name: $(cat /sys/class/graphics/fb0/name)" >> /home/webc/.startx.log

if [[ $(basename $(readlink /sys/class/graphics/fb0/device/driver)) =~ [iI]915 ]]; then
	echo "if" >> /home/webc/.startx.log
	# generate xorg.conf containing Intel GPU specific optimisations
	cat<<EOF > $xorg_conf_file_name
# Options to improve animations and video playback on Intel GPUs
# ie all Intel NUCs

Section "Device"
  Identifier "Intel Graphics"
  Driver "intel"
  Option "TearFree" "true"
  Option "AccelMethod" "sna"
EndSection
EOF
elif [[ $(cat /sys/class/graphics/fb0/name) =~ intel ]]; then
	echo "elif" >> /home/webc/.startx.log
	# generate xorg.conf containing Intel GPU specific optimisations
	cat<<EOF > $xorg_conf_file_name
# Options to improve animations and video playback on Intel GPUs
# ie all Intel NUCs

Section "Device"
  Identifier "Intel Graphics"
  Driver "intel"
  Option "TearFree" "true"
  Option "AccelMethod" "sna"
EndSection
EOF
else
	echo "else" >> /home/webc/.startx.log
	echo. > $xorg_conf_file_name
fi
echo "X11 xorg.conf: $(cat /etc/X11/xorg.conf)" >> /home/webc/.startx.log
cmdline_has debug || cat /etc/webc/xorg.conf >> $xorg_conf_file_name
echo "X11 xorg.conf: $(cat /etc/X11/xorg.conf)" >> /home/webc/.startx.log
exec su webc -c startx >>/home/webc/.xerrors 2>&1

